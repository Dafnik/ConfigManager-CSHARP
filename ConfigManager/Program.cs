﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigManager
{
    class Program
    {
        static void Main(string[] args)
        {
            ConfigManagerAPI cmapi = new ConfigManagerAPI("C:\\Users\\Krems\\Desktop\\config\\", "config.yml");

            cmapi.Set("test1", false);
            cmapi.Set("test2", "thisis");
            cmapi.Set("test3", 12);
            cmapi.Set("test4", ":leleee");

            Console.WriteLine("Test1: " + cmapi.GetBoolean("test1"));
            Console.WriteLine("Test3: " + cmapi.GetInt("test3"));
            Console.WriteLine("Test4: " + cmapi.GetString("test4"));

            cmapi.Set("test1", true);

            Console.ReadKey();

        }
    }
}
